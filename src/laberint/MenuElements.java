/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laberint;

import java.awt.Color;
import tauler.Element;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import tauler.Element.TipusElement;

/**
 *
 * @author JaumeFerrerGomila
 */
public class MenuElements extends JPanel {

    private final int BOTO_SIZE = 50;
    private BotoElement botoSeleccionat;
    private final MouseListener botoML;

    BotoElement[] botons;

    public MenuElements() {
        botoSeleccionat = null;
        TipusElement[] tipusArray = TipusElement.values();
        botons = new BotoElement[tipusArray.length];
        botoML = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
                BotoElement botoClicat = (BotoElement) me.getSource();
                if (botoSeleccionat == botoClicat) {
                    botoSeleccionat.actiu = false;
                    botoSeleccionat.setBackground(null);
                    botoSeleccionat = null;
                } else {
                    if (botoSeleccionat != null) {
                        botoSeleccionat.setBackground(null);
                        botoSeleccionat.actiu = false;
                    }
                    botoSeleccionat = botoClicat;
                    botoSeleccionat.setBackground(Color.yellow);
                    botoSeleccionat.actiu = true;
                }
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }
        };
        for (int i = 0; i < tipusArray.length; i++) {
            botons[i] = new BotoElement(tipusArray[i]);
            botons[i].addMouseListener(botoML);
            this.add(botons[i]);
        }
    }

    public Enum getElementSeleccionat() {
        return botoSeleccionat.element.getTipus();
    }

    public boolean hasElementSeleccionat() {
        return botoSeleccionat != null;
    }

    private class BotoElement extends JButton {

        Element element;
        private boolean actiu;

        public BotoElement(TipusElement tipus) {
            this.element = new Element(tipus);
            if (element.getTipus() == TipusElement.AGENT) {
                this.setText("AGENT");
            } else if (element.getTipus() == TipusElement.MONSTRE) {
                this.setText("MONSTRE");
            } else if (element.getTipus() == TipusElement.PRECIPICI) {
                this.setText("PRECIPICI");
            } else if (element.getTipus() == TipusElement.TRESOR) {
                this.setText("TRESOR");
            } else {
                this.setText("ELIMINA");
            }

        }

    }
}
