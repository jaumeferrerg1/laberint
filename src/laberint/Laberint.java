package laberint;

/**
 *
 * @author Llorenç i Jaume
 */
import java.awt.Color;
import tauler.Tauler;
import tauler.Casella;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import tauler.Element.TipusElement;
import static javax.swing.JOptionPane.showMessageDialog;
        
/**
 *
 * @author Llorens
 */
public class Laberint extends JFrame {

    JPanel panell;
    Tauler tauler;
    JButton botoStart;
    JButton botoRestaurar;
    MenuElements menu;

    MouseListener casellaML;

    LinkedList<Agent> llistaAgents;
    HashSet<Casella> posicioAgents;

    public Laberint(int dim) {
        super(" Laberint ");
        setSize(900, 950);
        llistaAgents = new LinkedList<>();
        posicioAgents = new HashSet<>();
        botoStart = new JButton();
        botoStart.setText(" >>>> START <<<< ");
        botoRestaurar = new JButton();
        botoRestaurar.setText("Restaurar tauler");
        panell = new JPanel();
        panell.setSize(900, 950);
        casellaML = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
                Casella casella = (Casella) me.getSource();
                if (menu.hasElementSeleccionat()) {
                    TipusElement elementSeleccionat = (TipusElement) menu.getElementSeleccionat();
                    if (casella.isAssignable()) {
                        if (elementSeleccionat != TipusElement.OK) {
                            tauler.setElement(casella, (TipusElement) menu.getElementSeleccionat());
                            if (elementSeleccionat == TipusElement.AGENT) {
                                posicioAgents.add(casella);
                            }
                            mouseEntered(me);
                        }
                    } else if (!casella.getElement().isPista()
                            && casella.getElement().getTipus() != TipusElement.OK
                            && elementSeleccionat == TipusElement.OK) {
                        if (casella.getElement().getTipus() == TipusElement.AGENT) {
                            posicioAgents.remove(casella);
                        }
                        tauler.setElement(casella, TipusElement.OK);
                    }
                    casella.repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                Casella casella = (Casella) me.getSource();
                if (menu.hasElementSeleccionat()) {
                    tauler.setPreview(casella, (TipusElement) menu.getElementSeleccionat());
                    tauler.repaint();
                }
            }

            @Override
            public void mouseExited(MouseEvent me) {
                Casella casella = (Casella) me.getSource();
                if (menu.hasElementSeleccionat()) {
                    tauler.setPreview(casella, null);
                    tauler.repaint();
                }
            }
        };
        botoStart.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (posicioAgents.isEmpty()) {
                    showMessageDialog(null, "Afegeix almenys un agent!");
                }
                llistaAgents.clear();
                posicioAgents.forEach((casella) -> {
                    Agent a = new Agent(llistaAgents.size(), tauler, casella);
                    llistaAgents.add(a);
                    casella.entraAgent(a);
                    tauler.setElement(casella, TipusElement.OK);
                    casella.repaint();
                });
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        botoRestaurar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                posicioAgents.clear();
                tauler.restaurar();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        tauler = new Tauler(dim, 800, casellaML);
        menu = new MenuElements();
        botoStart.setBackground(Color.green);
        botoStart.setSize(100, 0);
        panell.add(menu);
        panell.add(botoStart);
        panell.add(botoRestaurar);
        panell.add(tauler);
        getContentPane().add(panell);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Laberint laberint = new Laberint(10);
    }
}
