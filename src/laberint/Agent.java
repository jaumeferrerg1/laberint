/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laberint;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import tauler.Casella;
import tauler.Tauler;

/**
 *
 * @author jaume
 */
public class Agent {

    private final BufferedImage img;
    private final int id;

    public Agent(int id, Tauler tauler, Casella casellaInicial) {
        this.id = id;
        img = getImage(id);
        System.out.println("Hola, sóc agent amb casella inicial: " + casellaInicial.getI() + ", " + casellaInicial.getJ());
    }

    private static BufferedImage getImage(int id) {
        String imgPath = "img/agent" + id % 4 + ".png";
        BufferedImage i = null;
        try {
            i = ImageIO.read(new File(imgPath));
        } catch (IOException ex) {
            System.err.println("ERROR: no s'ha pogut llegir fixter: " + imgPath);
        }
        return i;
    }

    public void paintComponent(Graphics g, float f, float f0, float f1, float f2) {
        g.drawImage(img, (int) f, (int) f0, (int) f1, (int) f2, null);
    }

}
