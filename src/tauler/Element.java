package tauler;

/**
 *
 * @authors Llorenç i Jaume
 */
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Element {

    public static enum TipusElement {
        AGENT, MONSTRE, PRECIPICI, TRESOR, OK
    }

    public static enum TipusPista {
        PUDOR, BRISA, RESPLENDOR
    }

    private final BufferedImage img;

    private final boolean isPista;

    private final Enum tipus;

    public Enum getTipus() {
        return tipus;
    }

    public Element(TipusElement s) {
        img = getElementImg(s);
        tipus = s;
        isPista = false;
    }

    public Element(TipusPista s) {
        img = getElementImg(s);
        tipus = s;
        isPista = true;
    }

    public boolean isPista() {
        return this.isPista;
    }

    public static BufferedImage getElementImg(Enum s) {
        String imgPath = "img/" + s + ".png";
        try {
            return ImageIO.read(new File(imgPath));
        } catch (IOException ex) {
            System.err.println("ERROR: no s'ha pogut llegir fixter: " + imgPath);
        }
        return null;
    }

    public void paintComponent(Graphics g, float x, float y, float width, float height) {
        g.drawImage(img, (int) x, (int) y, (int) width, (int) height, null);
    }
}
