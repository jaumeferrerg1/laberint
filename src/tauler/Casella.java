package tauler;

/**
 *
 * @author Llorenç i Jaume
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import javax.swing.JPanel;
import laberint.Agent;
import tauler.Element.TipusElement;

public class Casella extends JPanel {

    private final Rectangle2D.Float rectangle;
    private Color color;
    private Element element, elementPreview;
    private final int i;
    private final int j;
    private boolean assignable;
    private final LinkedList<Agent> agents;

    // aquí pots afegir tots els atributs que tengui una casella (percepcions)
    public Casella(Rectangle2D.Float r, Color c, int i, int j) {
        this.rectangle = r;
        this.color = c;
        this.element = new Element(TipusElement.OK);
        this.i = i;
        this.j = j;
        assignable = true;
        agents = new LinkedList<>();
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(this.color);
        g2d.fill(this.rectangle);
        g2d.setColor(Color.BLACK);
        g2d.draw(rectangle);

        if (elementPreview != null) {
            this.elementPreview.paintComponent(g, rectangle.x + 1, rectangle.y + 1, rectangle.width - 1, rectangle.height - 1);
        } else if (this.hasElement()) {
            this.element.paintComponent(g, rectangle.x + 1, rectangle.y + 1, rectangle.width - 1, rectangle.height - 1);
        }
        if (!agents.isEmpty()) {
            agents.getLast().paintComponent(g, rectangle.x + 1, rectangle.y + 1, rectangle.width - 1, rectangle.height - 1);   
        }
    }

    public Element getElement() {
        return element;
    }

    protected void setColor(Color col) {
        this.color = col;
    }

    protected void setElement(Element s) {
        if (s.getTipus() == TipusElement.OK) {
            assignable = true;
        }
        this.element = s;
    }

    protected void setElementPreview(Element s) {
        this.elementPreview = s;
    }

    protected void setAssignable(boolean value) {
        this.assignable = value;
    }

    protected void restaurar() {
        if (element.getTipus() != TipusElement.OK) {
            element = new Element(TipusElement.OK);
        }
        assignable = true;
        this.repaint();
    }
    
    public void entraAgent(Agent a) {
        agents.add(a);
    }
    
    public void surtAgent(Agent a) {
        agents.remove(a);
    }

    public boolean hasElement() {
        return element != null && element.getTipus() != TipusElement.OK;
    }

    public boolean isAssignable() {
        return assignable;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }
}
