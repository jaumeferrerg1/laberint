package tauler;

/**
 *
 * @author Llorenç i Jaume
 */
import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import tauler.Element.TipusElement;
import tauler.Element.TipusPista;

public class Tauler extends JPanel {

    private static final Color GRIS = Color.LIGHT_GRAY;
    private final int dimensio;
    private final int midaCasella;
    private final Casella tauler[][];

    public Tauler(int dim, int mida, MouseListener ml) {
        dimensio = dim;
        midaCasella = mida / dim;
        tauler = new Casella[dim][dim];
        setLayout(new GridLayout(dim, dim));
        for (int j = 0; j < dimensio; j++) {
            for (int i = 0; i < dimensio; i++) {
                Rectangle2D.Float rec = new Rectangle2D.Float(0, 0, midaCasella - 1, midaCasella - 1);
                tauler[i][j] = new Casella(rec, GRIS, i, j);
                tauler[i][j].addMouseListener(ml);
                this.add(tauler[i][j]);
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        for (int j = 0; j < dimensio; j++) {
            for (int i = 0; i < dimensio; i++) {
                try {
                    tauler[i][j].paintComponent(g);
                } catch (NullPointerException e) {
                }
            }
        }
    }

    public void setElement(Casella casella, TipusElement tipusElement) {
        if (tipusElement != TipusElement.AGENT
                && tipusElement != TipusElement.TRESOR) {
            ArrayList<Casella> veinats = getVeinats(casella);
            TipusPista elementsVeinats = null;
            switch (tipusElement) {
                case MONSTRE:
                    elementsVeinats = TipusPista.PUDOR;
                    break;
                case PRECIPICI:
                    elementsVeinats = TipusPista.BRISA;
                    break;
            }
            if (elementsVeinats != null) {
                for (Casella c : veinats) {
                    c.setElement(new Element(elementsVeinats));
                }
            } else {
                if (casella.getElement().getTipus() != TipusElement.AGENT
                        && casella.getElement().getTipus() != TipusElement.TRESOR) {
                    veinats.forEach((c) -> {
                        c.setElement(new Element(TipusElement.OK));
                    });
                } else {
                    casella.setElement(new Element(TipusElement.OK));
                }
            }
        }
        casella.setElement(new Element(tipusElement));
    }

    public void setPreview(Casella casella, TipusElement tipusElement) {
        if (tipusElement != null) {
            casella.setElementPreview(new Element(tipusElement));
            boolean assignable = true;
            ArrayList<Casella> veinats = getVeinats(casella);
            veinats.add(casella);
            switch (tipusElement) {
                case MONSTRE:
                case PRECIPICI:
                    for (Casella veinat : veinats) {
                        if (veinat.hasElement()) {
                            veinat.setColor(Color.RED);
                            assignable = false;
                        } else {
                            veinat.setColor(Color.GREEN);
                        }
                    }
                    casella.setAssignable(assignable);
                    break;
                case AGENT:
                case TRESOR:
                    if (casella.hasElement()) {
                        casella.setColor(Color.RED);
                        casella.setAssignable(false);
                    } else {
                        casella.setColor(Color.GREEN);
                        casella.setAssignable(true);
                    }
                    break;
                default:
                    if (casella.getElement().isPista()) {
                        casella.setColor(Color.RED);
                    } else if (casella.getElement().getTipus() != TipusElement.OK) {
                        if (casella.getElement().getTipus() != TipusElement.AGENT
                                && casella.getElement().getTipus() != TipusElement.TRESOR) {
                            veinats.forEach((veinat) -> {
                                if (veinat.getElement().isPista() || veinat.equals(casella)) {
                                    veinat.setColor(Color.GREEN);
                                }
                            });
                        } else {
                            casella.setColor(Color.GREEN);
                        }
                    }
                    break;
            }

        } else {
            casella.setElementPreview(null);
            casella.setColor(GRIS);
            getVeinats(casella).forEach((veinat) -> {
                veinat.setColor(GRIS);
            });
        }
    }

    public void restaurar() {
        for (Casella[] fila : tauler) {
            for (Casella casella : fila) {
                casella.restaurar();
            }
        }
    }

    private ArrayList<Casella> getVeinats(Casella casella) {
        int i = casella.getI();
        int j = casella.getJ();
        ArrayList<Casella> veinats = new ArrayList<>();
        if (i > 0) {
            veinats.add(tauler[i - 1][j]);
        }
        if (j > 0) {
            veinats.add(tauler[i][j - 1]);
        }
        if (i < dimensio - 1) {
            veinats.add(tauler[i + 1][j]);
        }
        if (j < dimensio - 1) {
            veinats.add(tauler[i][j + 1]);
        }
        return veinats;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(midaCasella * dimensio, midaCasella * dimensio);
    }
}
